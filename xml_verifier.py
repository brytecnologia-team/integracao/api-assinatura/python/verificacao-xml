import requests
import json
import base64

XML_URL_SERVER = "https://fw2.bry.com.br/api/xml-verification-service/v1/signatures/verify"

HEADER = {'Authorization': 'Bearer insert-a-valid-token'}

def verify_token():
    aux_authorization = HEADER['Authorization'].split(' ')
    if(aux_authorization[1] == 'insert-a-valid-token'):
        print("Insert a valid token")
        return False
    else:
        return True
        

# Step 1: Load the signed document.

file = open("./xml-signature/AssinaturaXAdES-ADRC.xml", 'rb').read()

def verify_xml_signature():

    print("================Starting XML/XAdES signature verification... ================")
    print("")

    signatures = []
    signatures.append(('signatures[0][content]', file))

    verification_form = {
        'nonce': 1,
        'contentsReturn': 'false',
        'signatures[0][nonce]': 1,
    }

# Step 2: Send the request to the BRy verification API with the loaded document.

    response = requests.post(XML_URL_SERVER, data = verification_form, headers=HEADER, files=signatures)
    if response.status_code == 200:
        data = response.json()

# Step 3: Gets the information about the signature.

        print("JSON verification response: ", data)
        print("")

        number_of_verified_signatures = len(data['verificationStatus'])
        print("Number of verified signatures: ", number_of_verified_signatures)

        for i in range(number_of_verified_signatures):

            verified_signature = data['verificationStatus'][i]

            print("")
            print("General status: ", verified_signature['generalStatus'])
            print("Signature format: ", verified_signature['signatureFormat'])

            signature_status = verified_signature['signatureStatus'][0]

            try:
                print("Signature policy status: ", signature_status['signaturePolicyStatus'])
            except KeyError:
                print('')

            print("Signature status: ", signature_status['status'])
            print("Original file Base64 hash: ", signature_status['originalFileBase64Hash'])
            print("Signing time: ", signature_status['signingTime'])
            print("Verification reference type: ", signature_status['verificationReferenceType'])
            print("")


            try:
                signature_time_stamp_status = signature_status['signatureTimeStampStatus']
                if len(signature_time_stamp_status) > 0:
                    
                    print("Time stamp information: ")
                    
                    TIME_STAMPER_CERTIFICATE_INDEX = len(signature_time_stamp_status[0]['timestampChainStatus']['certificateStatusList']) - 1
                    TIME_STAMPER_CERTIFICATE = signature_time_stamp_status[0]['timestampChainStatus']['certificateStatusList'][TIME_STAMPER_CERTIFICATE_INDEX]

                    print("Time stamper: ", TIME_STAMPER_CERTIFICATE['certificateInfo']['subjectDN']['cn'] if TIME_STAMPER_CERTIFICATE else " ")
                    print("Time stamp status ", signature_time_stamp_status[0]['status'])
                    print("Time stamp date: ", signature_time_stamp_status[0]['timeStampDate'])
                    print("Verification reference date: ", signature_time_stamp_status[0]['verificationReference'])
                    print("Verification reference type: ", signature_time_stamp_status[0]['verificationReferenceType'])
                    print("Time stamp policy: ", signature_time_stamp_status[0]['timeStampPolicy'])
                    print("Time stamp content hash: ", signature_time_stamp_status[0]['timeStampContentHash'])
                    print("Time stamp serial number: ", signature_time_stamp_status[0]['timestampSerialNumber'])

            except KeyError:
                print("")


            signer_chain_status = signature_status['chainStatus']

            try:
                SIGNER_CERTIFICATE_INDEX = len(signer_chain_status['certificateStatusList']) - 1
                SIGNER_CERTIFICATE = signer_chain_status['certificateStatusList'][SIGNER_CERTIFICATE_INDEX]

                print("")
                print("Signer information:")
                print("Signer: ", SIGNER_CERTIFICATE['certificateInfo']['subjectDN']['cn'])
                print("General status of the certificate chain: ", signer_chain_status['status'])
                print("Signer certificate status: ", SIGNER_CERTIFICATE['status'])
                print("ICP-Brasil certificate: ", SIGNER_CERTIFICATE['pkiBrazil'])
                print("Initial validity date of the signer certificate: ", SIGNER_CERTIFICATE['certificateInfo']['validity']['notBefore'])
                print("Final validity date of the signer certificate: ", SIGNER_CERTIFICATE['certificateInfo']['validity']['notAfter'])


            except KeyError:

                print("Incomplete chain of signer: Could not verify signer certificate")


    else:
        print(response.text)


if(verify_token()):
    verify_xml_signature()

